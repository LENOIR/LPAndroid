package fr.atchisson.youguihio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Detail_carte extends AppCompatActivity {

    CarteBDD cbdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_carte);

        Integer id = getIntent().getIntExtra("pos", -1);

        cbdd = new CarteBDD(this);
        cbdd.open();

        Carte carte = cbdd.getCarteWithId(id+1);
        final TextView def = (TextView) findViewById(R.id.textView_detail_def);
        final TextView atk = (TextView) findViewById(R.id.textView_detail_atk);
        final TextView desc = (TextView) findViewById(R.id.textView_detail_description);
        def.setText("DEF : "+carte.getDef());
        atk.setText("ATK : "+carte.getAtk());
        desc.setText(carte.getDescription());

    }
}
