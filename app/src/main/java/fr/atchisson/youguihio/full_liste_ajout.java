package fr.atchisson.youguihio;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import fr.atchisson.youguihio.liste_accueil;

import java.util.ArrayList;

public class full_liste_ajout extends AppCompatActivity {

    ListView la_liste;
    ArrayList<String> cartes;
    ArrayAdapter<String> adapter;
    fr.atchisson.youguihio.CarteBDD cbdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_liste_ajout);

        cbdd = new fr.atchisson.youguihio.CarteBDD(this);
        cbdd.open();
        cartes = cbdd.getNameFullList();

        la_liste = (ListView) findViewById(R.id.listview_full);
        adapter = new ArrayAdapter<String>( full_liste_ajout.this, android.R.layout.simple_list_item_1, cartes);
        la_liste.setAdapter(adapter);

        la_liste.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long truc){
                cbdd.ajouterCarteToDeck(position + 1);
                cbdd.close();
                setResult(Activity.RESULT_OK, new Intent());
                finish();
            }
        });
    }
}
