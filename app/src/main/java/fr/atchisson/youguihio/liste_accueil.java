package fr.atchisson.youguihio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class liste_accueil extends AppCompatActivity {

    ListView la_liste;
    ArrayList<String> cartes;
    ArrayAdapter<String> adapter;
    fr.atchisson.youguihio.CarteBDD cbdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_accueil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cbdd = new fr.atchisson.youguihio.CarteBDD(this);
        cbdd.open();
        cartes = cbdd.getNameList();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(liste_accueil.this, full_liste_ajout.class);
                startActivityForResult(intent, 2);
            }
        });
        fab.setImageResource(R.drawable.ic_plus);

        la_liste = (ListView) findViewById(R.id.listeview_mes_cartes);
        adapter = new ArrayAdapter<String>(liste_accueil.this, android.R.layout.simple_list_item_1, cartes);
        la_liste.setAdapter(adapter);

        la_liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long truc) {

                Intent intent = new Intent(liste_accueil.this, Detail_carte.class);
                intent.putExtra("pos", position);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_liste_accueil, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        maj();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        maj();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        maj();
                    }
                }, 1000);
    }
    public void maj() {
        cartes.clear();
        for (String s: cbdd.getNameList()) {
            cartes.add(s);
        }
        adapter.notifyDataSetChanged();
        la_liste.refreshDrawableState();
        Log.d("i", "coucou");
    }
}