package fr.atchisson.youguihio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import fr.atchisson.youguihio.LaBase;

/**
 * Created by atchisson on 27/02/17.
 */

public class CarteBDD {
    private SQLiteDatabase bdd;
    private LaBase labase;

    public CarteBDD(Context c) {
        labase = new LaBase(c, "base.db", null, 4);
    }

    public void open() {
        bdd = labase.getWritableDatabase();
    }

    public void close() {
        labase.close();
    }

    public SQLiteDatabase get() {
        return bdd;
    }

    public long ajoutCarte(Carte carte) {
        ContentValues cv = new ContentValues();
        cv.put("id_card", carte.getId());
        return bdd.insert("mycards", null, cv);
    }

    public Carte getCarteWithTitre(String titre) {
        Cursor c = bdd.query("cards", new String[] {"Card_ID", "Name", "Description", "Level", "Atk", "Def"}, "Name LIKE "+titre, null, null, null, null);
        return cursorToCarte(c);
    }

    public Carte getCarteWithId(Integer id) {
        Cursor c = bdd.query("cards", new String[] {"Card_ID", "Name", "Description", "Level", "Atk", "Def"}, "Card_ID = "+id, null, null, null, null);
        return cursorToCarte(c);
    }

    private Carte cursorToCarte(Cursor c) {
        if (c.getCount() == 0) {
            return null;
        }
        c.moveToFirst();
        Carte carte = new Carte(c.getInt(0), c.getString(1), c.getString(2), c.getInt(4), c.getInt(5));
        return carte;
    }

    public ArrayList<String> getNameList() {
        ArrayList<String> res = new ArrayList<String>();

        final String MY_QUERY = "select Name from mycards m inner join cards c on m.id_card=c.CARD_ID";

        Cursor c = bdd.rawQuery(MY_QUERY, new String[]{});

        c.moveToFirst();
        while(!c.isAfterLast()) {
            res.add(c.getString(0)+"");
            c.moveToNext();
        }
        c.close();
        return res;
    }

    public ArrayList<String> getNameFullList() {
        ArrayList<String> res = new ArrayList<String>();

        Cursor c = bdd.query("cards", new String[] {"Name"}, null, null, null, null, null);

        c.moveToFirst();
        while(!c.isAfterLast()) {
            res.add(c.getString(0)+"");
            c.moveToNext();
        }
        c.close();
        return res;
    }

    public void ajouterCarteToDeck(int id) {
        ContentValues cv = new ContentValues();
        cv.put("id_card", id);
        bdd.insert("mycards", null, cv);
    }
}
