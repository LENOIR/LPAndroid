package fr.atchisson.youguihio;

/**
 * Created by atchisson on 27/02/17.
 */

public class Carte {
    private Integer id;
    private String name;
    private String description;
    private Integer atk;
    private Integer def;

    public Carte(Integer id, String name, String description, Integer atk, Integer def) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.atk = atk;
        this.def = def;
    }

    public Integer getId() {
        return id;
    }

    public Integer getDef() {
        return def;
    }

    public void setDef(Integer def) {
        this.def = def;
    }

    public Integer getAtk() {
        return atk;
    }

    public void setAtk(Integer atk) {
        this.atk = atk;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
